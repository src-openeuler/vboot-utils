%define _default_patch_fuzz 2
Name:                vboot-utils
Version:             20230127
Release:             2
Summary:             Chromium OS verified boot utility
License:             BSD
URL:                 https://chromium.googlesource.com/chromiumos/platform/vboot_reference
ExclusiveArch:       aarch64 x86_64 loongarch64
Source0:             %{name}-9b08a3c4.tar.xz

Patch0:	vboot-utils-9b08a3c4.patch
Patch1: flashrom-ensure-flashrom-symbols-are-not-loaded-if-U.patch

BuildRequires:       gcc-c++ openssl-devel trousers-devel libyaml-devel xz-devel libuuid-devel python3
%description
Verify boot is a collection of utilities for chromebook computers. Package and sign the kernel and
manage gpt partitions.

%prep
%autosetup -n vboot-utils-9b08a3c4 -p1

%build
%ifarch aarch64
%global ARCH arm
%endif
%ifarch x86_64
%global ARCH x86_64
%endif
%ifarch loongarch64
%global ARCH loongarch64
%endif
%make_build V=1 ARCH=%{ARCH} COMMON_FLAGS="$RPM_OPT_FLAGS" USE_FLASHROM=0

%install
%make_install V=1 DESTDIR=%{buildroot} ARCH=%{ARCH} COMMON_FLAGS="$RPM_OPT_FLAGS" USE_FLASHROM=0
mkdir -p %{buildroot}%{_datadir}/vboot/
cp -rf tests/devkeys %{buildroot}%{_datadir}/vboot/
rm -rf %{buildroot}/usr/lib/pkgconfig/
rm -rf %{buildroot}/usr/default/
rm -rf %{buildroot}/etc/default/
rm -rf %{buildroot}/usr/share/vboot/bin/
rm -f %{buildroot}/usr/lib/libvboot_host.a

%files
%doc README LICENSE
%{_bindir}/*
%{_datadir}/vboot/devkeys/

%changelog
* Tue Feb 27 2024 doupengda <doupengda@loongson.cn> - 20230127-2
- add support for loongarch64

* Wed Jul 19 2023 xu_ping <707078654@qq.com> - 20230127-1
- upgrade to 20230127

* Fri Jul 30 2021 sunguoshuai <sunguoshuai@huawei.com> - 20190823-6
- upgrade to 20190823

* Tue Oct 27 2020 Anan Fu <fuanan3@huawei.com> - 20180531-5
- disable python2

* Wed Jan 8 2020 leiju <leiju4@huawei.com> - 20180531-4
- fix unaligned point value with GCC9

* Wed Jan 8 2020 fengbing <fengbing7@huawei.com> - 20180531-3
- Package init
